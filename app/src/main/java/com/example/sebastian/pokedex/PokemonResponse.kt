package com.example.sebastian.pokedex

data class PokedexResponse (
        val count:Int?,
        val results:List<PokemonResponse>?
) {

  data class PokemonResponse(
          val name:String,
          val url:String,
          val id:Int,
          val height: Int,
          val weight: Int
  )
}

